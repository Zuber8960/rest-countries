import React, { useContext } from "react";
import { Link, useParams } from "react-router-dom";
import { ThemeContext } from "../../App";
import "./Country.css";

const CountryDetails = ({ countries }) => {
  const { id } = useParams();

  const [darkMode, setDarkMode] = useContext(ThemeContext);

  const country = countries.find((eachCountry) => eachCountry.cca3 === id);

  if (country === undefined) {
    return (
      <div
        className={
          (darkMode && "countryDetails activeDark") ||
          "countryDetails deactiveDark"
        }
      >
        <Link to="/">
          <button className="btn">Back</button>
        </Link>
        <h1>Country not found</h1>
      </div>
    );
  }
  const nativeName = country.name.nativeName
    ? Object.values(country.name.nativeName)[0].common
    : "No Native Name";

  //   console.log(Object.values(country.name.nativeName));

  const tld = country.tld && country.tld.length ? country.tld[0] : "";

  const lang = country.languages
    ? Object.values(country.languages).join(",")
    : "No Language";

  const sub = country.subregion ? country.subregion : "No Subregion";

  const currencies = country.currencies
    ? Object.values(country.currencies)
        .map((curr) => curr.name)
        .join(",")
    : "No currency";

  const borders =
    (country.borders &&
      country.borders.length &&
      country.borders.map((border) => {
        return (
          <Link to={`/countryId/${border}`} key={border}>
            <button className="btn border">{border}</button>
          </Link>
        );
      })) ||
    "No Border Countries";

  return (
    <div
      className={
        (darkMode && "countryDetails activeDark") ||
        "countryDetails deactiveDark"
      }
    >
      <Link to="/">
        <button className="btn">Back</button>
      </Link>
        <div className="countryData">
          <img src={country.flags.png} alt={country.name.common} />
          <div className="innerDetails">
            <div>
              <h1>{country.name.common}</h1>
            </div>
            <div className="innerChild">
              <div>
                <p>Native Name : {nativeName}</p>
                <p>Population : {country.population}</p>
                <p>Region : {country.region}</p>
                <p>Subregion : {sub}</p>
                <p>
                  Capital :
                  {(country.capital !== undefined &&
                    country.capital.join(",")) ||
                    " No Capital"}
                </p>
              </div>
              <div>
                <p>Top Lavel Domain : {tld}</p>
                <p>Currency : {currencies}</p>
                <p>Language : {lang}</p>
              </div>
            </div>
            <span>Borders Countries : {borders}</span>
          </div>
        </div>
    </div>
  );
};

export default CountryDetails;
