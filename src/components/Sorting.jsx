import React, {useContext} from "react";
import { ThemeContext } from "../App";

const Sorting = ({ setSelectedSort}) => {

  const [darkMode, setDarkMode] = useContext(ThemeContext);

 
  //when user choose any sorting

  function sortingBy(event) {
    if (event.target.value != "") {
      const condition = event.target.value.split(" ")[0];
      const sortedBy = event.target.value.split(" ")[1];
      setSelectedSort([condition, sortedBy]);
    }
  }

  return (
    <select id="subRegion" className={(darkMode && "activeDark") || "deactiveDark"} onChange={sortingBy}>
      <option value="" >Sort</option>
      <option value="descending population">Decending Sort(Population)</option>
      <option value="ascending population">Ascending Sort(Population)</option>
      <option value="descending area">Decending Sort(Area)</option>
      <option value="ascending area">Ascending Sort(Area)</option>
    </select>
  );
};

export default Sorting;