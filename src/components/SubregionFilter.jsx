import React, {useContext} from "react";
import { ThemeContext } from "../App";


const SubregionFilter = ({ arrayOfSubregion, setCurrentSubregion }) => {

  const [darkMode, setDarkMode] = useContext(ThemeContext);


  function changeSubRegion(event) {
    const subRegionName = event.target.value;
    setCurrentSubregion(subRegionName);
  };



  return (
    <select id="subRegion" onChange={changeSubRegion} className={(darkMode && "activeDark") || "deactiveDark"}>
      <option disabled>Filter by SubRegion</option>
      <option value="">All SubRegions</option>
      
      {arrayOfSubregion.length !== 0 && !arrayOfSubregion.includes(undefined) && arrayOfSubregion.map((sub) => {
        return (
          <option key={sub} value={sub}>
            {sub}
          </option>
        );
      })}
    </select>
  );
};

export default SubregionFilter;