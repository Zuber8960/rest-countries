import React, { useContext } from "react";
import { ThemeContext } from "../App";

const Header = () => {

  const [darkMode, setDarkMode] = useContext(ThemeContext);

  function setMode() {
    return setDarkMode((oldMode) => {
      return !oldMode;
    });
  }
  return (
    <nav className={(darkMode && "activeDark bottom-border") || "deactiveDark"}>
      <span className="largerFontSize bold">Where in the world?</span>
      <div className="headerDiv" onClick={setMode}>
        {(darkMode && <i className="fa-solid fa-circle-half-stroke"></i>) || (
          <i className="fa-regular fa-moon"></i>
        )}
        {(darkMode && <span>Light Mode</span>) || <span>Dark Mode</span>}
      </div>
    </nav>
  );
};

export default Header;
