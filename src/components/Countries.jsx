import React, { useContext } from "react";
import { ThemeContext } from "../App";
import { Link } from "react-router-dom";

const Counteries = ({ countries }) => {
  const [darkMode, setDarkMode] = useContext(ThemeContext);

  return (
    <ul
      className={
        (darkMode && "countries activeDark") || "countries deactiveDark"
      }
    >
      {(countries.length &&
        countries.map((country) => {
          return (
            <Link key={country.name.common} to={`/countryId/${country.cca3}`}>
              <li className="country">
                <img src={country.flags.png} alt={country.name.common} />
                <p className="bold bottomMargin">{country.name.common}</p>
                <p>Population : {country.population}</p>
                <p>Subregion : {country.subregion}</p>
                <p>Area : {country.area}</p>
                <p>
                  Capital :{" "}
                  {(country.capital !== undefined &&
                    country.capital.join(",")) ||
                    "no capital"}
                </p>
              </li>
            </Link>
          );
        })) || <li>No Country Data Founds</li>}
    </ul>
  );
};

// export { fetchedCountriesData };

export default Counteries;
