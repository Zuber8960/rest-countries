import React,{useContext} from "react";
import { ThemeContext } from "../App";


const Search = ({ setSearchedCountry}) => {

  const [darkMode, setDarkMode] = useContext(ThemeContext);

  function searchCountry(event) {
    setSearchedCountry(event.target.value);
  }

  return (
    <input
      type="search"
      className={(darkMode && "nosubmit activeDark") || "nosubmit deactiveDark"}
      id="search"
      name="Search"
      placeholder="Search Country Name..."
      onChange={searchCountry}
    />
  );
};

export default Search;
