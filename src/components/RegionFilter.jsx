import React, {useContext} from "react";
import { ThemeContext } from "../App";

const RegionFilter = ({setCurrentRegion, setCurrentSubRegion}) => {
  const [darkMode, setDarkMode] = useContext(ThemeContext);
  function changeRegion(event) {
    // console.log(event.target.value);

    const regionName = event.target.value;
    setCurrentRegion(regionName);
    setCurrentSubRegion("");

  }
  return (
    <select id="filter" onChange={changeRegion} className={(darkMode && "activeDark" ) || "deactiveDark"}>
      <option disabled>Filter by Region</option>
      <option value="">All Region</option>
      <option value="Africa">Africa</option>
      <option value="Americas">America</option>
      <option value="Asia">Asia</option>
      <option value="Europe">Europe</option>
      <option value="Oceania">Oceania</option>
      <option value="Antarctic">Antarctic</option>
    </select>
  );
};

export default RegionFilter;