import React, { useEffect, useState } from "react";

export const ThemeContext = React.createContext(null);

import Countries from "./components/Countries";
import Header from "./components/Header";
import RegionFilter from "./components/RegionFilter";
import Search from "./components/Search";
import SubregionFilter from "./components/SubregionFilter";
import Sorting from "./components/Sorting";
import { Routes, Route } from "react-router-dom";
import CountryDetails from "./components/CountryDetails/CountryDetails";

const App = () => {
  const [countries, setCountries] = useState([]);

  const [isLoader, setLoader] = useState(true);
  const [error, setError] = useState(null);
  const [darkMode, setDarkMode] = useState(false);

  const [searchedCountry, setSearchedCountry] = useState("");
  const [currentRegion, setCurrentRegion] = useState("");

  const [currentSubregion, setCurrentSubregion] = useState("");

  const [selectedSort, setSelectedSort] = useState([]);
  const arrayOfSubregion = [];

  useEffect(() => {
    const fetchCountrieData = async () => {
      try {
        const fetchedData = await fetch("https://restcountries.com/v3.1/all");
        if (fetchedData.status === 404) {
          console.error(fetchedData);
          setError(fetchedData.statusText);
        } else {
          const jsonData = await fetchedData.json();
          setLoader(false);
          setCountries(jsonData);
        }
      } catch (err) {
        console.error(err);
        setError(err.message);
      }
    };
    fetchCountrieData();
  }, []);

  let filteredCountriesData;

  filteredCountriesData = countries.filter((country) => {
    //setting subregion
    if (
      currentRegion !== "" &&
      currentRegion === country.region &&
      !arrayOfSubregion.includes(country.subregion)
    ) {
      arrayOfSubregion.push(country.subregion);
    }

    return (
      // when subregion is not selected
      (country.region.toLowerCase().includes(currentRegion.toLowerCase()) &&
        country.name.common
          .toLowerCase()
          .includes(searchedCountry.toLowerCase()) &&
        currentSubregion === "") ||
      // when subregion is selected
      (country.name.common
        .toLowerCase()
        .includes(searchedCountry.toLowerCase()) &&
        currentSubregion !== "" &&
        country.subregion !== undefined &&
        country.subregion
          .toLowerCase()
          .includes(currentSubregion.toLowerCase()))
    );
  });

  // function for sorting area or population by ascending or descending order.

  function sortingByCondition(dataOfCountries, condition, sortedBy) {
    if (condition === "ascending") {
      return [...dataOfCountries].sort(
        (first, second) => first[sortedBy] - second[sortedBy]
      );
    } else if (condition === "descending") {
      return [...dataOfCountries].sort(
        (first, second) => second[sortedBy] - first[sortedBy]
      );
    }
  }

  if (selectedSort.length) {
    filteredCountriesData = sortingByCondition(
      filteredCountriesData,
      ...selectedSort
    );
  }

  if (error) {
    return (
      <>
        <h1>Error : Internal server error</h1>
        <h1>Message : {error}</h1>
      </>
    );
  } else if (isLoader) {
    return (
      <div className="loaderClass">
        <div
          className={(isLoader && "loader active") || "loader deactive"}
        ></div>
      </div>
    );
  } else {
    return (
      <ThemeContext.Provider value={[darkMode, setDarkMode]}>
        <Header />

        <Routes>
          <Route
            path="/"
            element={
              <>
                <div
                  className={
                    (darkMode && "search-and-filter activeDark") ||
                    "search-and-filter dactiveDark"
                  }
                >
                  <Search setSearchedCountry={setSearchedCountry} />
                  <Sorting setSelectedSort={setSelectedSort} />
                  <SubregionFilter
                    arrayOfSubregion={arrayOfSubregion}
                    setCurrentSubregion={setCurrentSubregion}
                  />
                  <RegionFilter
                    countries={countries}
                    currentRegion={currentRegion}
                    setCurrentRegion={setCurrentRegion}
                    setCurrentSubRegion={setCurrentSubregion}
                  />
                </div>
                <Countries countries={filteredCountriesData} />
              </>
            }
          />
          <Route
            path="/countryId/:id"
            element={<CountryDetails countries={countries} />}
          />
        </Routes>
      </ThemeContext.Provider>
    );
  }
};

export default App;
